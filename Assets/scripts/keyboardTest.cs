﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class keyboardTest : MonoBehaviour {
    Text myText;
    Image myPanel;
  

    // Use this for initialization
    void Start() {
        GameObject playerPos = GameObject.Find("CameraParent");
        GameObject livingPos = GameObject.Find("livingRoom");
        GameObject kitchenPos = GameObject.Find("kitchen");
        playerPos.transform.position = livingPos.transform.position;
        //transform.position = kitchenPos.transform.position;
    }

    // Update is called once per frame
    void Update() {
        
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(0, 3, 0);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(0, -3, 0);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.Rotate(3, 0, 0);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Rotate(-3, 0, 0);
        }
      
    }
}
