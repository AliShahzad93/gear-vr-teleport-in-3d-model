using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace VRStandardAssets.Utils
{
    // This class should be added to any gameobject in the scene
    // that should react to input based on the user's gaze.
    // It contains events that can be subscribed to by classes that
    // need to know about input specifics to this gameobject.
    public class VRInteractiveItem : MonoBehaviour
    {
        public event Action OnOver;             // Called when the gaze moves over this object
        public event Action OnOut;              // Called when the gaze leaves this object
        public event Action OnClick;            // Called when click input is detected whilst the gaze is over this object.
        public event Action OnDoubleClick;      // Called when double click input is detected whilst the gaze is over this object.
        public event Action OnUp;               // Called when Fire1 is released whilst the gaze is over this object.
        public event Action OnDown;             // Called when Fire1 is pressed whilst the gaze is over this object.


        protected bool m_IsOver;
        Text myText;
        Text myExit;
        Image myPanel;
        Image myTap;


        public bool IsOver
        {
            get { return m_IsOver; }              // Is the gaze currently over this object?
        }
        //
        //
        // The below functions are called by the VREyeRaycaster when the appropriate input is detected.
        // They in turn call the appropriate events should they have subscribers.
        public void Over()
        {
            m_IsOver = true;

            if (OnOver != null)
                OnOver();
            Debug.Log("Veiwing " + this);
            prompt();
        }
        public void teleport()
        {
          
            GameObject playerPos = GameObject.Find("CameraParent");
            GameObject livingPos = GameObject.Find("livingRoom");
            GameObject kitchenPos = GameObject.Find("kitchen");
            GameObject bedPos = GameObject.Find("bedRoom");
            GameObject bathPos = GameObject.Find("bathRoom");
            if (playerPos.transform.position==livingPos.transform.position)
            {
                if(this.name=="WallKitchen")
                {
                     //playerPos.transform.position = kitchenPos.transform.position;
                   
                    StartCoroutine(locationChange(kitchenPos.transform));

                   StartCoroutine(locationDisplay("In Kitchen"));
                    //fade();

                }
                if (this.name == "exitLiving/bath")
                {
                    //playerPos.transform.position = bathPos.transform.position;
                    StartCoroutine(locationChange(bathPos.transform));
                    StartCoroutine(locationDisplay("In Bath Room"));

                }

                if (this.name == "exitLiving/bed")
                {
                    //playerPos.transform.position = bedPos.transform.position;
                    StartCoroutine(locationChange(bedPos.transform));
                    StartCoroutine(locationDisplay("In Bed Room"));

                }

            }
            if (playerPos.transform.position == kitchenPos.transform.position)
            {
                if (this.name == "WallLiving")
                {
                    //playerPos.transform.position = livingPos.transform.position;
                    StartCoroutine(locationChange(livingPos.transform));
                    StartCoroutine(locationDisplay("In Living Room"));
                }
                if (this.name == "exitLiving/bath")
                {
                    //playerPos.transform.position = bathPos.transform.position;
                    StartCoroutine(locationChange(bathPos.transform));
                    StartCoroutine(locationDisplay("In Bath Room"));

                }
                if (this.name == "exitLiving/bed")
                {
                    //playerPos.transform.position = bedPos.transform.position;
                    StartCoroutine(locationChange(bedPos.transform));
                    StartCoroutine(locationDisplay("In Bed Room"));

                }
                //if (this.name == "WallKitchen/Bath")
                //{
                //    playerPos.transform.position = bathPos.transform.position;
                //    StartCoroutine(locationDisplay("In Bath Room"));
                //}
            }
            if (playerPos.transform.position == bathPos.transform.position)
            {
                if (this.name == "exitBath/living")
                {
                    //playerPos.transform.position = livingPos.transform.position;
                    StartCoroutine(locationChange(livingPos.transform));
                    StartCoroutine(locationDisplay("In Living Room"));
                }
                //if (this.name.Equals( "WallKitchen/Bath"))
                //{
                //    Debug.Log("Bath to Kitchen clicked");
                //    playerPos.transform.position = kitchenPos.transform.position;
                //}

            }
            if (playerPos.transform.position == bedPos.transform.position)
            {

                if (this.name == "exitBed/living")
                {
                    //playerPos.transform.position = livingPos.transform.position;
                    StartCoroutine(locationChange(livingPos.transform));
                    StartCoroutine(locationDisplay("In Living Room"));
                }
                //if (this.name == "WallBed/Bath")
                //{
                //    playerPos.transform.position = bathPos.transform.position;
                //}

            }
        }
        public void prompt()
        {
            GameObject playerPos = GameObject.Find("CameraParent");
            GameObject livingPos = GameObject.Find("livingRoom");
            GameObject kitchenPos = GameObject.Find("kitchen");
            GameObject bedPos = GameObject.Find("bedRoom");
            GameObject bathPos = GameObject.Find("bathRoom");
            if (playerPos.transform.position == livingPos.transform.position)
            {
                if (this.name == "WallKitchen")
                {
                    promptDisplay("To Kitchen");
                }
                else if (this.name == "exitLiving/bath")
                {
                    promptDisplay("To Bath Room");
                }

                else if (this.name == "exitLiving/bed")
                {
                    promptDisplay("To Bed Room");
                }
                else
                    hidePrompt();
            }
            if (playerPos.transform.position == kitchenPos.transform.position)
            {
                if (this.name == "WallLiving")
                {
                    promptDisplay("To Living Room");
                }
                else if (this.name == "exitLiving/bath")
                {
                    promptDisplay("To Bath Room");
                }
                else if (this.name == "exitLiving/bed")
                {
                    promptDisplay("To Bed Room");
                }
                //else if(this.name == "WallKitchen/Bath")
                //{
                //    promptDisplay("To Bath Room");
                //}
                else
                    hidePrompt();
            
        }
            if (playerPos.transform.position == bathPos.transform.position)
            {
                if (this.name == "exitBath/living")
                {
                    promptDisplay("To Living Room");
                }
                else
                    hidePrompt();
            }
            if (playerPos.transform.position == bedPos.transform.position)
            {

                if (this.name == "exitBed/living")
                {
                    promptDisplay("To Living Room");
                }
                else
                    hidePrompt();
            }

            }
        public void promptDisplay(string message)
        {
            myPanel = GameObject.Find("Panel").GetComponent<Image>();
            if (myPanel.enabled == false)
            {
                myTap = GameObject.Find("tap").GetComponent<Image>();
                myTap.enabled = true;
                myExit = GameObject.Find("exitText").GetComponent<Text>();
                myExit.text = message;
            }
        }
        public void hidePrompt()
        {
            myTap = GameObject.Find("tap").GetComponent<Image>();
            myTap.enabled = false;
            myExit = GameObject.Find("exitText").GetComponent<Text>();
            myExit.text = "";
        }
       
        IEnumerator locationChange(Transform target)
        {
            GameObject playerPos = GameObject.Find("CameraParent");
            while (playerPos.transform.position !=target.position)
            {
                playerPos.transform.position = Vector3.Lerp(playerPos.transform.position, target.position, 10 * Time.deltaTime);
                yield return null;
            }
        }
        IEnumerator locationDisplay(string message)
        {
            hidePrompt();
            myPanel = GameObject.Find("Panel").GetComponent<Image>();
            myText = GameObject.Find("Text").GetComponent<Text>();
            myPanel.canvasRenderer.SetAlpha(0f);
            myText.canvasRenderer.SetAlpha(0f);
            myText.text = message;
            myPanel.enabled = true;
            myPanel.CrossFadeAlpha(1f, 1f, true);
            myText.CrossFadeAlpha(1f, 1f, true);
            yield return new WaitForSeconds(3);
            myText.text = " ";
            myPanel.enabled = false;

        }

        public void Out()
        {
            m_IsOver = false;

            if (OnOut != null)
                OnOut();
        }


        public void Click()
        {
            if (OnClick != null)
                OnClick();
             teleport();
     
        }


        public void DoubleClick()
        {
            if (OnDoubleClick != null)
                OnDoubleClick();
        }


        public void Up()
        {
            if (OnUp != null)
                OnUp();
        }


        public void Down()
        {
            if (OnDown != null)
                OnDown();
        }
    }
}